// import required React components
import React from 'react';
import ReactDOM from 'react-dom';

// Importing main app component
import App from './App';
import './styles.css';

// Keep the main container element in a variable
const app = document.getElementById('root');

// Rendering the App component inside the app element
ReactDOM.render(<App />, app);

// De esta forma agregamos un nuevo elemento
/* 
    let myNewP = document.createElement('p');
    myNewP.innerHTML = 'Esto es un párrafo.';
    app.append(myNewP);
*/
