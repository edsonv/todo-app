import React from 'react';

// Functional component
function TodoItem(props) {
    const completedStyle = {
        fontStyle: 'italic',
        color: '#cdcdcd',
        textDecoration: 'line-through'
    };
    return (
        <div className='todo-item'>
            <input
                type='checkbox'
                checked={props.item.completed}
                onChange={event => props.handleChange(props.item.id)}
            />
            <p style={props.item.completed ? completedStyle : null}>
                {props.item.text}
            </p>
        </div>
    );
}

// Class component
/* class TodoItem extends React.Component {
    render() {
        return (
            <div className='todo-item'>
                <input type='checkbox' checked={this.props.item.completed} />
                <p>{this.props.item.text}</p>
            </div>
        );
    }
} */

export default TodoItem;
